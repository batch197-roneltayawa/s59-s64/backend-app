const express = require('express');
const cors = require('cors')
const userRoutes = require('./routes/user.routes')
const productRoutes = require('./routes/product.routes')
const orderRoutes = require('./routes/order.routes')


const app = express();
const db = require('./data/database')

const port = process.env.PORT || 4001


// Middleware:
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use('/users', userRoutes)
app.use('/products', productRoutes)
app.use('/orders', orderRoutes)


app.listen(port, () => console.log(`API ins now online at port: ${port}`));