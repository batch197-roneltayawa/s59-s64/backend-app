const mongoose = require('mongoose');

mongoose.connect(`mongodb+srv://nel03:jonsera03@zuitt-batch197.r9sr8xv.mongodb.net/s42-s46?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

const dbConnect = mongoose.connection;

dbConnect.on('error', () => console.error('Connection Error'));
dbConnect.once('open', () => console.log('Connected to MongoDB!'));

module.exports = dbConnect;