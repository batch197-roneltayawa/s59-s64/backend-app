const express = require('express')
const productController = require('../controllers/product.controllers')
const auth = require('../auth')

const router = express.Router()

// Create a new Product (Admin Only)
router.post('/', auth.verify, productController.addProduct)
// Retrieve all Product
router.get('/all', auth.verify, productController.getAllProducts)
// Retrieve Active Product
router.get('/', productController.getActiveProducts)
// Retrieve Single Product
router.get('/:productId', productController.getSingleProduct)
// Update Product information (Admin Only)
router.put('/:productId', auth.verify, productController.updateTheProduct)
// Archive Product (Admin Only)
router.put('/archive/:productId', auth.verify, productController.archiveProduct)
router.put('/active/:productId', auth.verify, productController.activeProduct)


module.exports = router;